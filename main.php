<?php
include_once ('includes/BddConnexion.php');
include ('includes/header.php');
$ecole = $bdd->query("SELECT * FROM ecoles");
$roles= $bdd->query("SELECT * FROM roles");

if($_POST){

    $user_enter = trim($_POST['user']);
    $mdp_enter = trim($_POST['password']);
    $role = $_POST['role'];

    $mdp_enter = password_hash($mdp_enter,PASSWORD_DEFAULT);
    $insert_user = $bdd->prepare('INSERT INTO utilisateurs(NOM,MDP,ROLE_ID) VALUES (:username,:password,:role)');
    $insert_user->bindParam(':username',$user_enter, PDO::PARAM_STR);
    $insert_user->bindParam(':password',$mdp_enter, PDO::PARAM_STR);
    $insert_user->bindParam(':role',$role, PDO::PARAM_INT);
    $insert_user->execute();

    header('Location: main.php');

}
?>


<div class="container">
    <input hidden  id="ecoleHidden" value="">
    <input hidden id="classeHidden" value="">
    <div id="container_ecole">
        <div class="list_ecole">
            <section class="list-wrap">
                <input type="text" id="search-text" placeholder="Rechercher" class="search-box" onautocomplete="false" autocomplete="off">
                <ul id="list">
                    <?php
                    while($result = $ecole->fetch()){
                        echo "<li class='in' value='".$result['ID']."' onclick='AfficheClasse(this)'>".$result['NOM']."</li>";
                    }?>
                    <span class="empty-item">no results</span>
                </ul>
            </section>
        </div>
    </div>
</div>

<div id="dialog" class="list_eleve" title="Liste éleves">
</div>


<div id="dialog_eleve" class="add_eleve" title="Ajouter eleve"></div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

    <form  class="body-log" action="main.php" method="post" >
        <h1>S'inscrire</h1>
        <input type="text" name="user" placeholder="Utilisateur"/>
        <input type="password" name="password" placeholder="Mot de passe"/>
        <input type="submit" name="sing_in" value="S'inscrire"/>
        <div class="select">
            <select  id="slct" name="role">
                <option selected disabled>Role</option>
                <?php
                while($result = $roles->fetch()){
                    echo "<option value='".$result['ID']."'>".$result['NOM']."</option>";
                }?>
            </select>
        </div>
    </form>
