-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 31 juil. 2020 à 11:43
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `jesuisencours`
--

-- --------------------------------------------------------

--
-- Structure de la table `classes`
--

DROP TABLE IF EXISTS `classes`;
CREATE TABLE IF NOT EXISTS `classes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOM` varchar(255) NOT NULL,
  `ECOLE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ECOLE_ID` (`ECOLE_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `classes`
--

INSERT INTO `classes` (`ID`, `NOM`, `ECOLE_ID`) VALUES
(1, 'B2DEV', 1),
(2, 'B3DEV', 1),
(3, 'M1', 1),
(4, 'M2', 1),
(5, 'B1', 1),
(6, 'B2DEV', 2),
(7, 'B3DEV', 2),
(8, 'M1', 2),
(9, 'M2', 2),
(10, 'B1', 2),
(11, 'B2DEV', 3),
(12, 'B3DEV', 3),
(13, 'M1', 3),
(14, 'M2', 3),
(15, 'B1', 3),
(16, 'B2DEV', 4),
(17, 'B3DEV', 4),
(18, 'M1', 4),
(19, 'M2', 4),
(20, 'B1', 4);

-- --------------------------------------------------------

--
-- Structure de la table `ecoles`
--

DROP TABLE IF EXISTS `ecoles`;
CREATE TABLE IF NOT EXISTS `ecoles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOM` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ecoles`
--

INSERT INTO `ecoles` (`ID`, `NOM`) VALUES
(1, 'IMIE Angers'),
(2, 'IMIE Nantes'),
(3, 'IMIE Cean'),
(4, 'IMIE Paris');

-- --------------------------------------------------------

--
-- Structure de la table `eleves`
--

DROP TABLE IF EXISTS `eleves`;
CREATE TABLE IF NOT EXISTS `eleves` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRENOM` varchar(255) NOT NULL,
  `NOM` varchar(255) NOT NULL,
  `CLASSE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `CLASSE_ID` (`CLASSE_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `eleves`
--

INSERT INTO `eleves` (`ID`, `PRENOM`, `NOM`, `CLASSE_ID`) VALUES
(1, 'Anthonin', 'Dudilieu', 1),
(2, 'Charles', 'Beslot', 1),
(3, 'Benjamin', 'Deletang', 1),
(4, 'Jean', 'heude', 1);

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOM` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`ID`, `NOM`) VALUES
(1, 'ADMIN'),
(2, 'GESTIONNAIRE');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOM` varchar(255) NOT NULL,
  `MDP` longtext NOT NULL,
  `ROLE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ROLE_ID` (`ROLE_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`ID`, `NOM`, `MDP`, `ROLE_ID`) VALUES
(1, 'test', '$2y$10$PSxtwrX2KX/elRWeayJMt.8ZREeUHwWoBLuoyIhBkHNQ4GUlj0M0W', NULL),
(2, 'test', '$2y$10$3WSb9dS74He.4KuyT0ZRx.dn8ctv/GLh/Mure94VVPbTJwiymho6a', 2),
(3, '', '$2y$10$XgXUa4pikIgKXU1omYARwuFDXRDuqwvZDeXJzIVE4mIfzkwT8rBY6', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
