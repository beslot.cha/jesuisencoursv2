<?php
include_once "includes/BddConnexion.php";

if(isset($_POST['idEcole']) && !empty($_POST['idEcole'])){
    $id_classe = $_POST['idEcole'];

    $classes = $bdd->prepare('SELECT * FROM classes WHERE ECOLE_ID=:id  ');
    $classes->bindParam(':id',$id_classe,PDO::PARAM_INT);
    $classes->execute();

    $html_result= '';
    $html_result.='<div id="container_classe"><div class="list_classe"><section class="list-wrap">';
    $html_result.='<ul id="list">';
    foreach ($classes as $result){
        $html_result.='<li class="in" value="'.$result["ID"].'" onclick="AffichePopup(this)">'.$result["NOM"].'</li>';
    }
    $html_result.='</ul></section></div></div>';
    echo $html_result;
}

if(isset($_POST['idEleve']) && !empty($_POST['idEleve'])){
    $id_eleve = $_POST['idEleve'];

    $eleves = $bdd->prepare('SELECT * FROM eleves WHERE CLASSE_ID=:id  ');
    $eleves->bindParam(':id',$id_eleve,PDO::PARAM_INT);
    $eleves->execute();
    $html_result= ' ';

    if($eleves->rowCount()> 0){
        $html_result.='<div id="container_classe"><div class="list_classe"><section class="list-wrap">';
        $html_result.='<ul id="list">';
        foreach ($eleves as $result){
            $html_result.='<li class="in" value="'.$result["ID"].'" onclick="AffichePopup(this)">'.$result["NOM"].' '.$result["PRENOM"].'</li>';
        }
        $html_result.='</ul></section></div></div>';
    }else{
        $html_result .="Aucun eleve dans cette classe";
    }
        $html_result.='</ul></section></div></div>';

    echo $html_result;
}

if(isset($_POST['action']) && $_POST['action'] == 'addEleve'){
    $idClass = $_POST['classID'];
    $nom = $_POST['prenom'];
    $prenom = $_POST['nom'];

    $req = $bdd->prepare("Insert INTO eleves(PRENOM,NOM,CLASSE_ID) VALUES (:prenom,:nom,:classID)");
    $req->bindParam(":prenom",$prenom,PDO::PARAM_STR);
    $req->bindParam(":nom",$nom,PDO::PARAM_STR);
    $req->bindParam(":classID",$idClass,PDO::PARAM_INT);
    $req->execute();
}

