$(document).ready(function() {
    var jobCount = $('#list .in').length;
    $('.list-count').text(jobCount + ' items');


    $("#search-text").keyup(function () {
        //$(this).addClass('hidden');

        var searchTerm = $("#search-text").val();
        var listItem = $('#list').children('li');


        var searchSplit = searchTerm.replace(/ /g, "'):containsi('");

        //extends :contains to be case insensitive
        $.extend($.expr[':'], {
            'containsi': function(elem, i, match, array)
            {
                return (elem.textContent || elem.innerText || '').toLowerCase()
                    .indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });


        $("#list li").not(":containsi('" + searchSplit + "')").each(function(e)   {
            $(this).addClass('hiding out').removeClass('in');
            setTimeout(function() {
                $('.out').addClass('hidden');
            }, 300);
        });

        $("#list li:containsi('" + searchSplit + "')").each(function(e) {
            $(this).removeClass('hidden out').addClass('in');
            setTimeout(function() {
                $('.in').removeClass('hiding');
            }, 1);
        });


        var jobCount = $('#list .in').length;
        $('.list-count').text(jobCount + ' items');

        //shows empty state text when no jobs found
        if(jobCount == '0') {
            $('#list').addClass('empty');
            $('.empty-item').show();
        }
        else {
            $('#list').removeClass('empty');
            $('.empty-item').hide();
        }

        if(searchTerm == ""){
            var li =  $('#list li');
            for(var i =0; i<li.length;i++){
                $(li[i]).removeClass();
                $(li[i]).addClass('in');
                $('.empty-item').hide();
                $('.list-count').text(li.length + ' items');
            }
        }

    });
    $( "#dialog" ).dialog({
        autoOpen: false,
        show: {
            height :350,
            width : 650,
        },
    });

    $( "#dialog_eleve" ).dialog({
        autoOpen: false,
        autoOpen: false,
        height: 400,
        width: 350,
        modal: true,
        buttons: {
            "Valider": Valider,
            Retour: function() {
                $("#dialog_eleve").dialog("close");
            }
        },
        close: function() {
            form[ 0 ].reset();
            allFields.removeClass( "ui-state-error" );
        }
    });
});

function AfficheClasse(item){
    $("#ecoleHidden").val($(item).val());
    $("#container_classe").remove();
    $.ajax({
        url:'ajaxRequest.php',
        type: 'POST',
        data: 'idEcole=' + $(item).val()
    }).done(function (data) {
        // success
        $(".container").append(data);

    }).fail(function (data) {
        //error
        alert('Une erreur est survenue');
    });

}

function AffichePopup(item){
    $("#classeHidden").val($(item).val());
    $('#dialog').children().remove();
    $("#dialog").dialog("open");

    $.ajax({
        url:'ajaxRequest.php',
        type: 'POST',
        data: 'idEleve=' + $(item).val()
    }).done(function (data) {
        // success
        console.log(data);
        $('.list_eleve').append(data);
        $('.list_eleve').append('<button id="create-user" onclick="addEleve()">Ajouter Eleve</button>');
    }).fail(function (data) {
        //error
        alert('Une erreur est survenue');
    });
}

function addEleve() {
    $("#dialog_eleve").dialog("open");
    $("#dialog_eleve").children().remove();
    $('.add_eleve').append(CreateForm());
}

function CreateForm() {
    var form =  '<form>\n' +
        '    <fieldset>\n' +
        '      <label for="name">Nom</label>\n' +
        '      <input type="text" name="nom" id="nom" value="" class="text ui-widget-content ui-corner-all">\n' +
        '     <label for="name">Prenom</label>\n' +
        '  <input type="text" name="prenom" id="prenom" value="" class="text ui-widget-content ui-corner-all">\n' +
        '      <!-- Allow form submission with keyboard without duplicating the dialog button -->\n' +
        '      <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">\n' +
        '    </fieldset>\n' +
        '  </form>';
    return form;
}

function Valider() {
    let prenom= $('#prenom').val();
    let nom = $('#nom').val();
    let classe = $("#classeHidden").val();
    $.ajax({
        url:'ajaxRequest.php',
        type: 'POST',
        data:{
            'action': 'addEleve',
            'prenom': prenom,
            'nom': nom,
            'classID' : classe

    }
    }).done(function (data) {
        // success
        alert("Eleve correctement ajouté");
        $("#dialog_eleve").dialog("close");
        $("#dialog").dialog("close");
    }).fail(function (data) {
        //error
        alert('Une erreur est survenue');
    });
}